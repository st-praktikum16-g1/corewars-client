package gone.client.model.network;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.client.model.contracts.PlayerData;
import gone.client.model.visual.Instructions;
import gone.client.model.visual.IpOwner;
import gone.lib.common.ClientRole;
import gone.lib.common.RcStandard;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwStartContent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;


/**
 * @author matthias
 */
public class HandleServerAnswersTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testHandleServerAnswers() throws UnknownHostException {
        PlayerData playerData = new PlayerData("name", ClientRole.Player);
        HandleServerAnswers handleServerAnswers = new HandleServerAnswers(playerData);

        assertEquals(handleServerAnswers.getPlayerData(), playerData);
        assertFalse(handleServerAnswers.isCoreIsInitiated());
        assertEquals(handleServerAnswers.getPlayerCount(), 2);
        assertEquals(handleServerAnswers.getLineLength(), 100);
        assertEquals(handleServerAnswers.getRcStandardServer(), RcStandard.ICWS88);
    }

    @Test
    public void testGetVisCore_CoreNotInitiated() throws UnknownHostException, IllegalAccessException {
        thrown.expect(IllegalAccessException.class);
        PlayerData playerData = new PlayerData("name", ClientRole.Player);
        HandleServerAnswers handleServerAnswers = new HandleServerAnswers(playerData);
        handleServerAnswers.getVisualizationCore();
    }

    @Test
    public void getInstructionType() throws UnknownHostException {
        HandleServerAnswers temp = new HandleServerAnswers(new PlayerData("name", ClientRole.Player));

        assertEquals(Instructions.CORE_DAT, temp.getInstructionType("CORE_DAT"));
        assertEquals(Instructions.USER_DAT, temp.getInstructionType("USER_DAT"));

        assertEquals(Instructions.ADD, temp.getInstructionType("ADD"));
        assertEquals(Instructions.MOV, temp.getInstructionType("MOV"));
        assertEquals(Instructions.SUB, temp.getInstructionType("SUB"));
        assertEquals(Instructions.JMP, temp.getInstructionType("JMP"));
        assertEquals(Instructions.JMZ, temp.getInstructionType("JMZ"));

        assertEquals(Instructions.JMN, temp.getInstructionType("JMN"));
        assertEquals(Instructions.DJN, temp.getInstructionType("DJN"));
        assertEquals(Instructions.CMP, temp.getInstructionType("CMP"));
        assertEquals(Instructions.SPL, temp.getInstructionType("SPL"));
        assertEquals(Instructions.SLT, temp.getInstructionType("SLT"));

        // thrown.expect(IllegalArgumentException.class);
        // hotfix from winfried: getInstructionType returns core_dat instead of exception
        temp.getInstructionType("üäö");
    }

    @Test
    public void receiveLoginOkTelegram() throws UnknownHostException, IllegalAccessException {
        List<String> warrCode = new LinkedList<>();
        warrCode.add("JMP 0 1");
        warrCode.add("JMP 2 1");
        HandleServerAnswers tempHandle = new HandleServerAnswers(
                new PlayerData("name", ClientRole.Player, "localhost", 1111, warrCode));

        CwLoginOkContent loginOk = new CwLoginOkContent();
        loginOk.clientMustWait = false;
        loginOk.lineLength = 999;
        loginOk.coreSize = 50000;
        loginOk.standard = RcStandard.ICWS88;

        // need to simulate that the client is connected … done by connect() on CwClient
        tempHandle.getPlayerData().setConnectedToServer(true);

        tempHandle.receiveLoginOkTelegram(loginOk);
        assertTrue(tempHandle.getPlayerData().isReadyToSendWarriorCode());
        assertEquals(tempHandle.getVisualizationCore().getSize(), loginOk.coreSize);
        assertTrue(tempHandle.isCoreIsInitiated());
        assertEquals(tempHandle.getLineLength(), loginOk.lineLength);
        assertEquals(tempHandle.getRcStandardServer(), loginOk.standard);
    }

    @Test
    public void receiveStartTelegram() throws UnknownHostException, IllegalAccessException {
        List<String> warrCode = new LinkedList<>();
        warrCode.add("JMP 0 1");
        HandleServerAnswers tempHandle = new HandleServerAnswers(
                new PlayerData("name", ClientRole.Player, "localhost", 1111, warrCode));

        CwLoginOkContent loginOk = new CwLoginOkContent();
        loginOk.clientMustWait = false;
        loginOk.lineLength = 999;
        loginOk.coreSize = 3;
        loginOk.standard = RcStandard.ICWS88;

        // need to simulate that the client is connected … done by connect() on CwClient
        tempHandle.getPlayerData().setConnectedToServer(true);
        tempHandle.receiveLoginOkTelegram(loginOk);

        // receive start telegram
        CwStartContent cwStart = new CwStartContent();
        cwStart.players = 2;

        List<String> fullChanges = new LinkedList<>();
        fullChanges.add("JMP");
        fullChanges.add("CORE_DAT");
        fullChanges.add("MOV");

        cwStart.fullState = fullChanges;
        tempHandle.receiveStartTelegram(cwStart);

        assertEquals(2, tempHandle.getPlayerCount());

        assertEquals(3, tempHandle.getVisualizationCore().getSize());
        assertEquals(tempHandle.getVisualizationCore().getElement(0).getInstruction(), Instructions.JMP);
        assertEquals(tempHandle.getVisualizationCore().getElement(1).getInstruction(), Instructions.CORE_DAT);
        assertEquals(tempHandle.getVisualizationCore().getElement(2).getInstruction(), Instructions.MOV);
    }

    @Test
    public void receiveGameStatusTelegram() throws UnknownHostException, IllegalAccessException {
        List<String> warrCode = new LinkedList<>();
        warrCode.add("JMP 0 1");
        HandleServerAnswers tempHandle = new HandleServerAnswers(
                new PlayerData("name", ClientRole.Player, "localhost", 1111, warrCode));

        CwLoginOkContent loginOk = new CwLoginOkContent();
        loginOk.clientMustWait = false;
        loginOk.lineLength = 999;
        loginOk.coreSize = 3;
        loginOk.standard = RcStandard.ICWS88;

        // need to simulate that the client is connected … done by connect() on CwClient
        tempHandle.getPlayerData().setConnectedToServer(true);
        tempHandle.receiveLoginOkTelegram(loginOk);

        // receive start telegram
        CwStartContent cwStart = new CwStartContent();
        cwStart.players = 2;

        List<String> fullChanges = new LinkedList<>();
        fullChanges.add("JMP");
        fullChanges.add("CORE_DAT");
        fullChanges.add("MOV");

        cwStart.fullState = fullChanges;
        tempHandle.receiveStartTelegram(cwStart);

        // receive game status update
        CwGameStatusContent cwStatus = new CwGameStatusContent();
        cwStatus.indexOfChangedCoreElement = 1;
        cwStatus.oldIpIndex = 1;
        cwStatus.newIpIndex = 2;
        cwStatus.newOpCode = "ADD";
        cwStatus.playerOneActive = true;

        assertEquals(IpOwner.NEUTRAL, tempHandle.getVisualizationCore().getElement(1).getIpOwner());
        assertEquals(Instructions.CORE_DAT, tempHandle.getVisualizationCore().getElement(1).getInstruction());

        tempHandle.receiveGameStatusTelegram(cwStatus);

        assertEquals(IpOwner.NEUTRAL, tempHandle.getVisualizationCore().getElement(0).getIpOwner());
        assertEquals(Instructions.JMP, tempHandle.getVisualizationCore().getElement(0).getInstruction());

        assertEquals(IpOwner.NEUTRAL, tempHandle.getVisualizationCore().getElement(1).getIpOwner());
        assertEquals(Instructions.ADD, tempHandle.getVisualizationCore().getElement(1).getInstruction());

        assertEquals(IpOwner.PLAYER1, tempHandle.getVisualizationCore().getElement(2).getIpOwner());
        assertEquals(Instructions.MOV, tempHandle.getVisualizationCore().getElement(2).getInstruction());
    }
}
