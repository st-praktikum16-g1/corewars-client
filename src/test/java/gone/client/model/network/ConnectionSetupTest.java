package gone.client.model.network;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.client.model.contracts.PlayerData;
import gone.lib.common.ClientRole;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;


/**
 * @author matthias
 */
public class ConnectionSetupTest {
    private HandleServerAnswers handleServerAnswers;
    private ConnectionSetup connectionSetup;
    private PlayerData playerData;
    private List<String> warriorCode;
    private static final int SERVER_PORT = 9090;
    private ServerSocket socket;

    @Before
    public void setUp() throws IOException {
        warriorCode = new LinkedList<>();
        warriorCode.add("JMP 0 1");
        warriorCode.add("MOV 3 1");
        warriorCode.add("ADD 2 1");
        playerData = new PlayerData("name", ClientRole.Player, "localhost", SERVER_PORT,  warriorCode);
        socket = new ServerSocket(SERVER_PORT, 0, InetAddress.getByName("localhost"));
        handleServerAnswers = new HandleServerAnswers(playerData);
        connectionSetup = new ConnectionSetup(handleServerAnswers);
    }

    @Test
    public void testConnect() throws UnknownHostException {
        connectionSetup.connect();

        assertTrue(handleServerAnswers.getPlayerData().isConnectedToServer());
        assertEquals(handleServerAnswers.getPlayerData().getServerAddress(),
                InetAddress.getByName("localhost"));
        assertEquals(handleServerAnswers.getPlayerData().getServerPort(), SERVER_PORT);
    }

    @Test
    public void testDisconnect() throws UnknownHostException {
        connectionSetup.connect();
        connectionSetup.disconnect();

        assertFalse(handleServerAnswers.getPlayerData().isConnectedToServer());
        assertFalse(handleServerAnswers.getPlayerData().isReadyToSendWarriorCode());
    }

    @Test
    public void testSendLogin() {
        connectionSetup.connect();
        connectionSetup.sendLogin();
    }

    @Test
    public void testSendReady() {
        connectionSetup.connect();
        connectionSetup.sendLogin();
        connectionSetup.sendReady();
    }

    @After
    public void cleanUp() throws IOException {
        socket.close();
    }
}
