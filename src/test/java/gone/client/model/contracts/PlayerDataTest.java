package gone.client.model.contracts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.lib.common.ClientRole;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;


/**
 * @author matthias
 */
public class PlayerDataTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();


    @Test
    public void playerData_1() throws UnknownHostException {
        PlayerData toTest = new PlayerData("testName", ClientRole.Player);
        List<String> tempList = new LinkedList<>();
        tempList.add("JMP 0 1");
        tempList.add("MOV 0 1");
        tempList.add("ADD 0 1");

        toTest.setWarriorCode(tempList);
        toTest.setServerAddress("google.de");
        toTest.setServerPort(65432);

        assertEquals("testName", toTest.getName());
        assertEquals(ClientRole.Player, toTest.getRole());
        assertEquals("google.de", toTest.getServerAddress().getHostName());
        assertEquals(65432, toTest.getServerPort());
        assertFalse(toTest.isConnectedToServer());
        assertFalse(toTest.isReadyToSendWarriorCode());

        // warrior code
        assertEquals(toTest.getWarriorCode().get(0), "JMP 0 1");
        assertEquals(toTest.getWarriorCode().get(1), "MOV 0 1");
        assertEquals(toTest.getWarriorCode().get(2), "ADD 0 1");
        assertEquals(3, toTest.getWarriorCode().size());
    }

    @Test
    public void playerData_2() throws UnknownHostException {
        List<String> tempList = new LinkedList<>();
        tempList.add("DAT #0 #1");
        tempList.add("JMP 3 1");

        PlayerData toTest = new PlayerData("Bernd Wiesacher", ClientRole.Player, "reddit.com",
                123, tempList);
        toTest.setConnectedToServer(true);

        assertEquals("Bernd Wiesacher", toTest.getName());
        assertEquals(ClientRole.Player, toTest.getRole());
        assertEquals("reddit.com", toTest.getServerAddress().getHostName());
        assertEquals(123, toTest.getServerPort());
        assertTrue(toTest.isConnectedToServer());
        assertFalse(toTest.isReadyToSendWarriorCode());

        // warrior code
        assertEquals(2, toTest.getWarriorCode().size());
        assertEquals(toTest.getWarriorCode().get(0), "DAT #0 #1");
        assertEquals(toTest.getWarriorCode().get(1), "JMP 3 1");
    }

    @Test
    public void setName() throws UnknownHostException {
        PlayerData toTest = new PlayerData("testName", ClientRole.Player);
        assertEquals("testName", toTest.getName());

        toTest.setName("neuerName");
        assertEquals("neuerName", toTest.getName());
    }

    @Test
    public void setRole() throws UnknownHostException {
        PlayerData toTest = new PlayerData("testName", ClientRole.Player);
        assertEquals(ClientRole.Player, toTest.getRole());

        toTest.setRole(ClientRole.Viewer);
        assertEquals(ClientRole.Viewer, toTest.getRole());

        List<String> tempList = new LinkedList<>();
        tempList.add("DAT #0 #1");

        toTest.setWarriorCode(tempList);
        assertFalse(toTest.getWarriorCode().isEmpty());

        toTest.setReadyToSendWarriorCode(true);
        assertFalse(toTest.isReadyToSendWarriorCode());
    }

    @Test
    public void setEmptyName() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        new PlayerData("", ClientRole.Player);
    }

    @Test
    public void setServerPort_BadPortValue() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        List<String> tempList = new LinkedList<>();
        tempList.add("DAT #0 #1");
        new PlayerData("kurt", ClientRole.Player, "reddit.com", -1, tempList);
    }

    @Test
    public void setServerPort_UnknownHost() throws UnknownHostException {
        thrown.expect(UnknownHostException.class);
        List<String> tempList = new LinkedList<>();
        tempList.add("DAT #0 #1");
        new PlayerData("kurt", ClientRole.Player, "333.333.333.333", 1, tempList);
    }

    @Test
    public void setServerPort_EmptyWarriorList() throws UnknownHostException {
        thrown.expect(IllegalArgumentException.class);
        List<String> tempList = new LinkedList<>();
        tempList.add("DAT #0 #1");
        new PlayerData("kurt", ClientRole.Player, "reddit.com", 1, new LinkedList<>());
    }
}
