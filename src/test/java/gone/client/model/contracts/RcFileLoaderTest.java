package gone.client.model.contracts;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.util.List;

/**
 * @author matthias
 */
public class RcFileLoaderTest {
    private SecureRandom random;
    private File customDirRc;
    private File goneCoreWars;
    private String pathCw;
    private String pathClientRc;
    private String dwarfPath;
    private File outputFile;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    // help function to generate random strings (file name)
    private String nextRandomString() {
        return new BigInteger(130, random).toString(32);
    }

    @Before
    public void saveRcFile() {
        random = new SecureRandom();
    }

    @Test
    public void testRcFileLoader() throws IOException, IllegalAccessException {
        String rcFilePath = nextRandomString() + ".txt";
        File createdFile = folder.newFile(rcFilePath);

        // write to temp file
        Writer outputWrite = Files.newBufferedWriter(createdFile.toPath(), Charset.defaultCharset());
        outputWrite.write("ADD #4, 3" + System.getProperty("line.separator"));
        outputWrite.write("MOV 2, @2" + System.getProperty("line.separator"));
        outputWrite.write("JMP -2" + System.getProperty("line.separator"));
        outputWrite.write("DAT #0, #0" + System.getProperty("line.separator"));
        outputWrite.close();

        // use rcLoader methods
        RcFileLoader fileLoader = new RcFileLoader(1000, createdFile);
        List<String> dataFromFile = fileLoader.getInputData();

        assertEquals("ADD #4, 3", dataFromFile.get(0));
        assertEquals("MOV 2, @2", dataFromFile.get(1));
        assertEquals("JMP -2", dataFromFile.get(2));
        assertEquals("DAT #0, #0", dataFromFile.get(3));
    }

    @Test
    public void testRcFileLoader_FileDoesNotExist() throws IOException, IllegalAccessException {
        thrown.expect(IllegalArgumentException.class);
        String rcFilePath = nextRandomString() + "111.txt";
        File createdFile = folder.newFile(rcFilePath);

        // use rcLoader methods
        RcFileLoader fileLoader = new RcFileLoader(1000, createdFile);
    }

    @Test
    public void testRcFileLoader_MaxLength() throws IOException, IllegalAccessException {
        thrown.expect(IllegalArgumentException.class);
        String rcFilePath = nextRandomString() + ".txt";
        File createdFile = folder.newFile(rcFilePath);

        // write to temp file
        Writer outputWrite = Files.newBufferedWriter(createdFile.toPath(), Charset.defaultCharset());
        outputWrite.write("ADD #4, 3" + System.getProperty("line.separator"));
        outputWrite.write("ADD #4, 3" + System.getProperty("line.separator"));
        outputWrite.write("ADD #4, 3" + System.getProperty("line.separator"));
        outputWrite.close();
        RcFileLoader fileLoader = new RcFileLoader(5, createdFile);
    }


    @Test
    public void setMaxLength_BadValue1() throws IOException {
        thrown.expect(IllegalArgumentException.class);
        new RcFileLoader(-1, folder.newFile(nextRandomString()));
    }

    @Test
    public void setMaxLength_BadValue2() throws IOException {
        thrown.expect(IllegalArgumentException.class);
        new RcFileLoader(999999999, folder.newFile(nextRandomString()));
    }
}
