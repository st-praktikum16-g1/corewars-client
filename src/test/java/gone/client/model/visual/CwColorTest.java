package gone.client.model.visual;

import static org.junit.Assert.assertEquals;

import javafx.scene.paint.Color;

import org.junit.Test;

/**
 * @author matthias
 */
public class CwColorTest {

    @Test
    public void colors_1() {
        CwColor testBlack = CwColor.CORE_DAT;
        assertEquals(testBlack, CwColor.CORE_DAT);

        CwColor testRed = CwColor.USER_DAT;
        assertEquals(testRed, CwColor.USER_DAT);

        CwColor testBlue = CwColor.INSTRUCTION;
        assertEquals(testBlue, CwColor.INSTRUCTION);

        CwColor testYellow = CwColor.IP1;
        assertEquals(testYellow, CwColor.IP1);
        assertEquals(testYellow.getColor(), Color.YELLOW);

    }
}
