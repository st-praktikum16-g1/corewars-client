package gone.client.model.visual;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author matthias
 */
public class CoreContentTest {

    @Test
    public void getColor() {
        CoreContent temp = new CoreContent(Instructions.ADD, IpOwner.PLAYER1);
        assertEquals(CwColor.INSTRUCTION, temp.getColorToElement());

        CoreContent coreDat = new CoreContent(Instructions.CORE_DAT, IpOwner.NEUTRAL);
        assertEquals(CwColor.CORE_DAT, coreDat.getColorToElement());

        CoreContent userDat = new CoreContent(Instructions.USER_DAT, IpOwner.PLAYER1);
        assertEquals(CwColor.USER_DAT, userDat.getColorToElement());
    }

    @Test
    public void ipOwnerString() {
        assertEquals("PLAYER1", IpOwner.PLAYER1.toString());
        assertEquals("PLAYER2", IpOwner.PLAYER2.toString());
        assertEquals("NEUTRAL", IpOwner.NEUTRAL.toString());
    }
}
