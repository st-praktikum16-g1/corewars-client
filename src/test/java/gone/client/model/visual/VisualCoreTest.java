package gone.client.model.visual;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.LinkedList;


/**
 * @author matthias
 */
public class VisualCoreTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testVisualizationCore_negativeSize() {
        thrown.expect(IllegalArgumentException.class);
        new VisualizationCore(-10);
    }

    @Test
    public void testVisualizationCore() {
        VisualizationCore temp = new VisualizationCore(10);

        for (int i = 0; i < temp.getSize(); i++) {
            assertEquals(temp.getElement(i).getInstruction(), Instructions.CORE_DAT);
            assertEquals(temp.getElement(i).getIpOwner(), IpOwner.NEUTRAL);
        }

        assertEquals(temp.getSize(), 10);
    }

    @Test
    public void testVisualCore_Observable() {
        VisualizationCore core = new VisualizationCore(10);
        assertFalse(core.hasChanged());

        core.sendUpdates(new LinkedList<>());
        assertFalse(core.hasChanged());
    }

    @Test
    public void testSetInstrType() {
        VisualizationCore tempCore = new VisualizationCore(2);

        assertEquals(tempCore.getElement(0).getInstruction(), Instructions.CORE_DAT);
        assertEquals(tempCore.getElement(1).getInstruction(), Instructions.CORE_DAT);

        tempCore.setInstrType(1, Instructions.ADD);
        assertEquals(tempCore.getElement(1).getInstruction(), Instructions.ADD);
        assertEquals(tempCore.getElement(0).getInstruction(), Instructions.CORE_DAT);
    }

    @Test
    public void testSetInstrPointer() {
        VisualizationCore tempCore = new VisualizationCore(3);

        assertEquals(tempCore.getElement(0).getIpOwner(), IpOwner.NEUTRAL);
        assertEquals(tempCore.getElement(1).getIpOwner(), IpOwner.NEUTRAL);
        assertEquals(tempCore.getElement(2).getIpOwner(), IpOwner.NEUTRAL);

        tempCore.setInstrPointer(1, IpOwner.PLAYER1);
        tempCore.setInstrPointer(2, IpOwner.PLAYER2);

        assertEquals(tempCore.getElement(0).getIpOwner(), IpOwner.NEUTRAL);
        assertEquals(tempCore.getElement(1).getIpOwner(), IpOwner.PLAYER1);
        assertEquals(tempCore.getElement(2).getIpOwner(), IpOwner.PLAYER2);
    }

    @Test
    public void testSendUpdates() {
        VisualizationCore core = new VisualizationCore(10);
        assertFalse(core.hasChanged());

        core.sendUpdates(new LinkedList<>());
        assertFalse(core.hasChanged());

        // TODO: we need to implement the observer to test this effectively (receive the list)
    }
}
