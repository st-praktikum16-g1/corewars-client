/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gone.client;

import gone.client.model.contracts.PlayerData;
import gone.client.model.network.ConnectionSetup;
import gone.client.view.SceneLoader;
import gone.lib.common.ClientRole;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 *
 * @author Benedikt Geißler
 */
public class Main extends Application {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        ConnectionSetup.getInstance().getHandleServerAnswers()
                .setPlayerData(new PlayerData("Player1", ClientRole.Player));

        SceneLoader.primaryStage = primaryStage;
        SceneLoader.primaryStage.setTitle("Core Wars");
        SceneLoader.primaryStage.getIcons().add(SceneLoader.ICON);

        FXMLLoader main = SceneLoader.loadFxml(getClass()
                .getResource("view/Main.fxml"));
        SceneLoader.rootLayout = main.load();

        FXMLLoader center = SceneLoader.loadFxml(getClass()
                .getResource("view/Login.fxml"));
        ((BorderPane) SceneLoader.rootLayout).setCenter(center.load());
        SceneLoader.showStage();
    }
}