package gone.client.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * SceneLoader is a class with some help methods containing some "boilerplate" code as well as
 * some static fields for the current scene and stage.
 *
 * @author benedikt
 */
public class SceneLoader {
    // TODO: load language from properties
    private static Locale locale = new Locale("en");
    private static ResourceBundle languageResource = ResourceBundle
            .getBundle("gone.client.view.locale.Main", locale);
    public static Stage primaryStage;
    public static Parent rootLayout;
    public static final Image ICON = new Image(SceneLoader.class.getResourceAsStream("images/logo.png"));

    /**
     * help function to load a explicit fxml file from a specified path
     * @param resource the path to the resource
     * @return a FXMLLoader object which represents the loaded fxml as a resource
     */
    public static FXMLLoader loadFxml(URL resource) {
        FXMLLoader result = new FXMLLoader();
        result.setLocation(resource);
        result.setResources(ResourceBundle.getBundle("gone.client.view.locale.Main", locale));
        return result;
    }

    /**
     * help function to visualize a stage. width and height can be set
     * MinHeight and MinWidth make sure the stage has useful dimensions
     */
    public static void showStage() {
        // TODO: load width and height from preferences (config)
        Scene scene = new Scene(rootLayout, 640, 480);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(300);
        primaryStage.setMinWidth(360);
        primaryStage.show();
    }

    public static String getLocaleString(String key) {
        return languageResource.getString(key);
    }

    /**
     * shows a alert message to communicate to the user what went wrong
     * @param message the error message
     * @param title the title of the stage
     */
    public static void showAlert(String message, String title) {
        Stage loginStage = new Stage();
        loginStage.setWidth(320);
        loginStage.setHeight(240);
        loginStage.setTitle(title);

        GridPane parent = new GridPane();

        parent.add(new Label(message), 0, 0);

        Scene scene = new Scene(parent, 320, 240);
        loginStage.setScene(scene);
        loginStage.showAndWait();
    }

    /**
     * opens a dialogue for the user to load text/redcode files
     * @return returns the loaded File object
     */
    public static File showFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(getLocaleString("redcode.choose"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(getLocaleString("file.type.redcode"), "*.rc",
                        "*.red"),
                new FileChooser.ExtensionFilter(getLocaleString("file.type.text"), "*.txt"),
                new FileChooser.ExtensionFilter(getLocaleString("file.type.all"), "*.*"));
        Stage stage = new Stage();
        File result = fileChooser.showOpenDialog(stage);
        return result;
    }
}