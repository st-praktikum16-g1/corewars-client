package gone.client.view;

/**
 * Created by winfr on 30.09.2016.
 */
public class ShowAlert implements Runnable {
    private String message;
    private String title;

    public ShowAlert(String message, String title) {
        this.message = message;
        this.title = title;
    }

    @Override
    public void run() {
        SceneLoader.showAlert(message, title);
    }
}
