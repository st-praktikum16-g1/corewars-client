package gone.client.model.visual;

/**
 * data type used for the content changes at the VisualizationCore
 * this data type is used to create arguments that can be sent
 * and received inside the observer pattern (VisualizationCore, UI)
 *
 * @author matthias
 */
public class CoreContentChange {
    public int indexOfChange;
    public CoreContent changedCoreContent;

    public CoreContentChange(int indexOfChange, CoreContent changedCoreContent) {
        this.indexOfChange = indexOfChange;
        this.changedCoreContent = changedCoreContent;
    }
}
