package gone.client.model.visual;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * visual representation of the server core for the UI
 * besides of that, the VisualizationCore is a Observable
 * (subject) - so changes on that subject can be send to Observers
 *
 *  @author matthias
 */
public class VisualizationCore extends Observable {
    private List<CoreContent> visualCore;

    /* constructor */

    /**
     * constructor
     * @param coreSize the size of the core
     * @throws IllegalArgumentException if the value is negative or zero
     */
    public VisualizationCore(int coreSize) throws IllegalArgumentException {
        if (coreSize <= 0) {
            throw new IllegalArgumentException("coreSize must be > 0 !");
        }
        this.visualCore = new LinkedList<>();

        for (int i = 0; i < coreSize; i++) {
            visualCore.add(new CoreContent(Instructions.CORE_DAT, IpOwner.NEUTRAL));
        }
    }

    /* setter */

    public void setInstrType(int index, Instructions instrType) throws IllegalArgumentException {
        getElement(index).setInstruction(instrType);
    }

    public void setInstrPointer(int index, IpOwner ipOwner) {
        getElement(index).setIpOwner(ipOwner);
    }

    /* getter */

    public int normalizeIndex(int index) {
        while (index < 0) {
            index += visualCore.size();
        }
        return index % visualCore.size();
    }

    public CoreContent getElement(int index) {
        return visualCore.get(normalizeIndex(index));
    }

    public int getSize() {
        return visualCore.size();
    }

    /* class methods */

    /**
     * this method will be called to send updates on this subject to
     * the observer (observer pattern)
     * @param changesForOutput a list with actual changes that have to be sent
     */
    public void sendUpdates(List<CoreContentChange> changesForOutput) {

        setChanged();

        if (!changesForOutput.isEmpty()) {
            notifyObservers(changesForOutput);
        }

        clearChanged();
    }
}
