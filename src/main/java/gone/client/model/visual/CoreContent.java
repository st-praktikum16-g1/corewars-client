package gone.client.model.visual;

/**
 * data type: real data at the visual core to
 * store information to create a representative
 * output at the UI
 *
 * @author matthias
 */
public class CoreContent {
    private Instructions instruction;
    private IpOwner ipOwner;

    /* constructor */

    /**
     * constructor
     * @param instruction the type of instruction
     * @param ipOwner status about instruction pointer setting at this CoreContent
     */
    public CoreContent(Instructions instruction, IpOwner ipOwner) {
        setInstruction(instruction);
        setIpOwner(ipOwner);
    }

    /* setter */

    public void setInstruction(Instructions value) {
        this.instruction = value;
    }

    public void setIpOwner(IpOwner value) {
        this.ipOwner = value;
    }

    /* getter */

    public Instructions getInstruction() {
        return instruction;
    }

    public IpOwner getIpOwner() {
        return ipOwner;
    }

    /**
     * simple help function to get the corresponding color to the
     * instruction type of this CoreContent
     * @return explicit color value (CwColor)
     */
    public CwColor getColorToElement() {
        CwColor cwColor;

        switch (this.getInstruction()) {
            case CORE_DAT:
                cwColor = CwColor.CORE_DAT;
                break;

            case USER_DAT:
                cwColor = CwColor.USER_DAT;
                break;

            default:
                cwColor = CwColor.INSTRUCTION;
                break;
        }
        return cwColor;
    }
}
