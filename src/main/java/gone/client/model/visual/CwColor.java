package gone.client.model.visual;

import javafx.scene.paint.Color;

/**
 * simple data type to store color value for the output at the UI
 *
 * @author matthias
 */
public enum CwColor {
    CORE_DAT(Color.BLACK),
    USER_DAT(Color.RED),
    INSTRUCTION(Color.BLUE),
    IP1(Color.YELLOW),
    IP2(Color.WHITE);

    private Color color;

    CwColor(int red, int green, int blue) {
        this.color = Color.rgb(red, green, blue);
    }

    CwColor(Color colorValue) {
        this.color = colorValue;
    }

    public Color getColor() {
        return color;
    }
}
