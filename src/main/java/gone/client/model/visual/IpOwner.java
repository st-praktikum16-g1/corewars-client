package gone.client.model.visual;

/**
 * represents to which player belongs the instruction pointer
 * NEUTRAL: no instruction pointer at that field in the VisualizationCore
 *
 * @author matthias
 */
public enum IpOwner {
    PLAYER1,
    PLAYER2,
    NEUTRAL
}
