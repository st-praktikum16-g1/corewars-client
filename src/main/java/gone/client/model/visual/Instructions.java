package gone.client.model.visual;

/**
 * instruction types representing the redcode instructions for CoreContent
 *
 * @author matthias
 */
public enum Instructions {
    MOV,
    ADD,
    SUB,
    JMP,
    JMZ,
    JMN,
    DJN,
    CMP,
    SPL,
    SLT,
    CORE_DAT,
    USER_DAT
}
