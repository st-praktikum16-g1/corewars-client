package gone.client.model.contracts;

import gone.lib.common.ClientRole;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

/**
 * data that gets generated through input at the ui
 * and the game procedure will be stored here
 *
 * @author matthias
 */
public class PlayerData {
    private String name;
    private ClientRole role;
    private boolean readyToSendWarriorCode;
    private boolean connectedToServer;
    private InetAddress serverAddress;
    private int serverPort;
    private List<String> warriorCode;

    /* constructor */

    /**
     * constructor
     * @param name warrior name
     * @param role client role
     * @param serverAddress address the client wants to connect to
     * @param serverPort tcp port for the connection to the server
     * @param warriorCode list to hold the instructions of the users rc file
     * @throws UnknownHostException if the server address can not be resolved
     */
    public PlayerData(String name, ClientRole role, String serverAddress,
                      int serverPort, List<String> warriorCode) throws UnknownHostException {
        setName(name);
        setRole(role);
        setReadyToSendWarriorCode(false);
        setConnectedToServer(false);
        setServerAddress(serverAddress);
        setServerPort(serverPort);
        setWarriorCode(warriorCode);
    }

    /**
     * constructor #2
     * @param name warrior name
     * @param role client role
     * @throws UnknownHostException if the server address can not be resolved
     */
    public PlayerData(String name, ClientRole role) throws UnknownHostException {
        setName(name);
        setRole(role);
        setReadyToSendWarriorCode(false);
        setConnectedToServer(false);
        setServerAddress("localhost");
        setServerPort(65432);
        this.warriorCode = new LinkedList<>();
    }

    /* setter */

    public void setServerAddress(String serverAddress) throws UnknownHostException {
        this.serverAddress = InetAddress.getByName(serverAddress);
    }

    public void setServerPort(int serverPort) throws IllegalArgumentException {
        if (serverPort < 0 || serverPort > 65535) {
            throw new IllegalArgumentException("server port must be >= 0 && <= 65535");
        }
        this.serverPort = serverPort;
    }

    public void setConnectedToServer(boolean connectedToServer) {
        this.connectedToServer = connectedToServer;
    }

    public void setWarriorCode(List<String> value) throws IllegalArgumentException {
        if (value.isEmpty()) {
            throw new IllegalArgumentException("warrior code list should not be empty");
        } else {
            this.warriorCode = value;
        }
    }

    public void setReadyToSendWarriorCode(boolean value) {
        // only set readyToSendWarriorCode, if warriorCode is set
        if (value) {
            if (this.warriorCode.isEmpty() || !connectedToServer) {
                return;
            }
        }
        readyToSendWarriorCode = value;
    }

    public void setName(String value) throws IllegalArgumentException {
        if (value.isEmpty()) {
            throw new IllegalArgumentException("name should not be empty");
        }
        this.name = value;
    }

    public void setRole(ClientRole value) {
        this.role = value;
    }

    /* getter */

    public boolean isReadyToSendWarriorCode() {
        return readyToSendWarriorCode;
    }

    public List<String> getWarriorCode() {
        return warriorCode;
    }

    public String getName() {
        return name;
    }

    public ClientRole getRole() {
        return role;
    }

    public boolean isConnectedToServer() {
        return connectedToServer;
    }

    public InetAddress getServerAddress() {
        return serverAddress;
    }

    public int getServerPort() {
        return serverPort;
    }
}
