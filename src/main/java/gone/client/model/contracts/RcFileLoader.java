package gone.client.model.contracts;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * reads a rc file (raw text) as a input stream from a relative path
 * Standard Charsets  is UTF-8
 *
 *  @author matthias
 */
public class RcFileLoader {
    private static int MAX_LENGTH;
    private List<String> inputData;

    /* constructor */

    /**
     * constructor
     * @param maxLength maximum length of the input file
     * @param file file object the gets loaded from a file chooser
     * @throws IOException if the file can not be found
     * @throws IllegalArgumentException if the maximum length value is to big
     */
    public RcFileLoader(int maxLength, File file) throws IOException, IllegalArgumentException {
        inputData = null;
        setMaxLength(maxLength);
        readFileFromPath(file);
    }

    /* setter */

    private void setInputData(List<String> value) throws  IllegalArgumentException {
        if (value.isEmpty()) {
            throw new IllegalArgumentException("input stream is empty");
        }
        this.inputData = value;
    }

    public void setMaxLength(int value) throws IllegalArgumentException {
        if (value <= 0 || value > 20000) {
            throw new IllegalArgumentException("maximum length to read must be > 0 && <= 2.000!");
        }
        MAX_LENGTH = value;
    }

    /* getter */

    public List<String> getInputData() throws IllegalAccessException {
        return inputData;
    }

    /* class methods */

    /**
     * reads a file object, checks existence & length, transfer it into list of strings
     * @param file the file objects that has to be read
     * @throws IOException if the file path does not exist
     */
    private void readFileFromPath(File file) throws IOException {
        if (!file.exists()) {
            throw new IllegalArgumentException("file does not exist");
        } else if (file.length() > MAX_LENGTH) {
            throw new IllegalArgumentException("file length is > " + MAX_LENGTH);
        }

        Path filePath = Paths.get(file.getPath());
        List<String> temp = Files.readAllLines(filePath);
        setInputData(temp);
    }
}
