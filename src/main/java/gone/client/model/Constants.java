package gone.client.model;

/**
 * here we store important constant values, which can be used globally
 *
 * @author benedikt
 */
public final class Constants {
    public static final int MAX_FILE_LENGTH = 10000;
}
