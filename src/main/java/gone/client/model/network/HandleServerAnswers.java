package gone.client.model.network;

import gone.client.model.contracts.PlayerData;
import gone.client.model.visual.CoreContent;
import gone.client.model.visual.CoreContentChange;
import gone.client.model.visual.Instructions;
import gone.client.model.visual.IpOwner;
import gone.client.model.visual.VisualizationCore;
import gone.client.view.SceneLoader;
import gone.client.view.ShowAlert;
import gone.lib.common.RcStandard;
import gone.lib.network.contracts.IClientCallback;
import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwStartContent;
import javafx.application.Platform;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

/**
 * process the answers from the server/tcp-module,
 * update objects on client side, if needed:
 * we store/set relevant data such as PlayerData and the VisualizationCore
 *
 * @author matthias
 */
public class HandleServerAnswers implements IClientCallback {
    private PlayerData playerData;
    private VisualizationCore visualizationCore;
    private boolean coreIsInitiated;
    private int playerCount;
    private int lineLength;
    private RcStandard rcStandardServer;

    /* constructor */

    /**
     * constructor
     * @param playerData the initial playerData object that was created earlier
     * @throws UnknownHostException if the server address of playerData is not
     */
    public HandleServerAnswers(PlayerData playerData) throws UnknownHostException {
        setPlayerData(playerData);
        this.coreIsInitiated = false;
        // default values, get updated later on
        this.playerCount = 2;
        this.lineLength = 100;
        this.rcStandardServer = RcStandard.ICWS88;
    }

    /**
     * empty constructor, we need it because the connectionSetup is implemented as
     * a singleton class … there we need to call this
     */
    public HandleServerAnswers() {
        this.coreIsInitiated = false;
        // default values, get updated later on
        this.playerCount = 2;
        this.lineLength = 100;
        this.rcStandardServer = RcStandard.ICWS88;
    }

    /* class methods */

    /**
     * receive login ok messages, update the ready boolean for the playerData
     * because now the client has a successful connection to the server ->
     * we now are able to send warrior code. the visualizationCore can be initiated
     * because we know how large it needs to be. update lineLength and rcStandardServer
     * @param cwLoginOkContent the incoming LoginOk telegram from the server
     */
    @Override
    public void receiveLoginOkTelegram(CwLoginOkContent cwLoginOkContent) {
        System.out.print("received login ok\n");
        if (!cwLoginOkContent.clientMustWait) {
            playerData.setReadyToSendWarriorCode(true);
        }

        // init visual core with core size
        if (!coreIsInitiated) {
            this.visualizationCore = new VisualizationCore(cwLoginOkContent.coreSize);
            this.coreIsInitiated = true;
        }

        // simply save parameters, maybe we can use them later
        this.lineLength = cwLoginOkContent.lineLength;
        this.rcStandardServer = cwLoginOkContent.standard;
    }

    /**
     * here we get a FULL update of the server core (init.), since the players warrior code
     * was copied to the server core. we update our visualizationCore, which is a
     * primitive representation of the server core state. we also save the player count.
     * the visualizationCore will send these updates to the observer (UI)
     * @param cwStartContent the incoming LoginOk telegram from the server
     */
    @Override
    public void receiveStartTelegram(CwStartContent cwStartContent) {
        try {
            System.out.print("received start\n");
            List<CoreContentChange> changesForOutput = new LinkedList<>();
            List<String> fullCoreUpdate = cwStartContent.fullState;

            for (int i = 0; i < fullCoreUpdate.size(); i++) {
                Instructions tempInstruction = getInstructionType(fullCoreUpdate.get(i));
                if (tempInstruction != Instructions.CORE_DAT) {
                    visualizationCore.setInstrType(i, tempInstruction);
                    CoreContent tempCore = visualizationCore.getElement(i);
                    changesForOutput.add(new CoreContentChange(i, tempCore));
                }
            }

            if (cwStartContent.players != 1 && cwStartContent.players != 2) {
                throw new IllegalArgumentException("player count has to be 1 or 2 !");
            }

            this.playerCount = cwStartContent.players;
            // send updates to observer
            visualizationCore.sendUpdates(changesForOutput);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * this method will be called to continuously update visualizationCore
     * with the updates from each game step at the server core
     * the visualizationCore will send these updates to the observer (UI)
     * @param cwGameStatusContent incoming game status update message
     */
    @Override
    public void receiveGameStatusTelegram(CwGameStatusContent cwGameStatusContent) {
        try {
            System.out.print("received game status\n");
            List<CoreContentChange> changesForOutput = new LinkedList<>();
            int oldIp = visualizationCore.normalizeIndex(cwGameStatusContent.oldIpIndex);
            int newIp = visualizationCore.normalizeIndex(cwGameStatusContent.newIpIndex);
            boolean playerOneIp = cwGameStatusContent.playerOneActive;

            // update instruction pointers
            IpOwner ipOwner = (playerOneIp) ? IpOwner.PLAYER1 : IpOwner.PLAYER2;
            visualizationCore.setInstrPointer(newIp, ipOwner);
            CoreContent tempCore = visualizationCore.getElement(newIp);
            changesForOutput.add(new CoreContentChange(newIp, tempCore));

            if (oldIp != -1 && oldIp != newIp) {
                visualizationCore.setInstrPointer(oldIp, IpOwner.NEUTRAL);
                tempCore = visualizationCore.getElement(oldIp);
                changesForOutput.add(new CoreContentChange(oldIp, tempCore));
            }

            // update instruction type at index of change
            if(cwGameStatusContent.indexOfChangedCoreElement >= 0) {
                Instructions instructionType = getInstructionType(cwGameStatusContent.newOpCode);
                int idxChange = visualizationCore.normalizeIndex(cwGameStatusContent.indexOfChangedCoreElement);
                visualizationCore.setInstrType(idxChange, instructionType);
                tempCore = visualizationCore.getElement(idxChange);
                changesForOutput.add(new CoreContentChange(idxChange, tempCore));
            }

            // send updates to observer
            visualizationCore.sendUpdates(changesForOutput);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void receiveGameResultTelegram(CwGameResultContent telegram) {
        if(telegram.state.equals("draw")) {
            Platform.runLater(new ShowAlert("Unentschieden", "Spielende"));
        } else {
            Platform.runLater(new ShowAlert("Spieler " + telegram.state +
                    " gewinnt", " Spielende"));
        }
    }

    /**
     * help function to receive the correct instruction type to the given op code
     * @param newOpCode given op code as string
     * @return fitting instruction type for the core
     */
    public Instructions getInstructionType(String newOpCode) {
        try {
            return Instructions.valueOf(newOpCode);
        } catch(Exception exception) {
            return Instructions.CORE_DAT;
        }
    }

    /* setter */

    public void setPlayerData(PlayerData playerData) {
        this.playerData = playerData;
    }

    /* getter */

    public boolean isCoreIsInitiated() {
        return coreIsInitiated;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public int getLineLength() {
        return lineLength;
    }

    public RcStandard getRcStandardServer() {
        return rcStandardServer;
    }

    public PlayerData getPlayerData() {
        return playerData;
    }

    public VisualizationCore getVisualizationCore() throws IllegalAccessException {
        if (!coreIsInitiated) {
            throw new IllegalAccessException("core is not initiated!");
        }
        return visualizationCore;
    }
}
