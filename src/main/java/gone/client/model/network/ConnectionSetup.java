package gone.client.model.network;

import gone.client.model.contracts.PlayerData;
import gone.lib.network.CwClient;
import gone.lib.network.contracts.IClientCallback;
import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwReadyContent;

import java.net.UnknownHostException;
import javax.naming.OperationNotSupportedException;


/**
 * use CwClient to setup a connection to the game server
 * this class is implemented as singleton to give
 * all classes access to the respective data
 *
 * @author matthias
 */
public class ConnectionSetup {
    private CwClient cwClient;
    private static ConnectionSetup instance;

    /* constructor */

    /**
     * constructor
     * @param callback with the callback object we can receive answers sent by the server
     */
    public ConnectionSetup(IClientCallback callback) {
        this.cwClient = new CwClient(callback);
    }

    /* class methods */

    /**
     * necessary method for the singleton pattern to receive one explicit instance
     * @return this explicit instance
     * @throws UnknownHostException gets thrown from the HandleServerAnswers
     */
    public static ConnectionSetup getInstance() throws UnknownHostException {
        if (instance == null) {
            instance = new ConnectionSetup(new HandleServerAnswers());
        }
        return instance;
    }

    /**
     * simple help function to get the HandleServerAnswers variable
     * @return this HandleServerAnswers variable
     */
    public HandleServerAnswers getHandleServerAnswers() {
        return (HandleServerAnswers) this.cwClient.getCallback();
    }

    /**
     * get the server address and port from the playerData value inside
     * HandleServerAnswers object, and connect to the specified server
     * use the functionality from the CwClient class
     */
    public void connect() {
        HandleServerAnswers tempHandle = getHandleServerAnswers();
        PlayerData tempPlayer = tempHandle.getPlayerData();

        String serverAddress = tempPlayer.getServerAddress().getHostAddress();
        int serverPort = tempPlayer.getServerPort();
        try {
            cwClient.connect(serverAddress, serverPort);
        } catch (OperationNotSupportedException exc) {
            exc.printStackTrace();
        }

        tempPlayer.setConnectedToServer(true);
        System.out.print("connected succesfully\n");
    }

    /**
     * simply disconnect from the server, update the boolean values at the
     * intern playerData variable
     */
    public void disconnect() {
        cwClient.disconnect();

        HandleServerAnswers tempHandle = getHandleServerAnswers();
        PlayerData tempPlayer = tempHandle.getPlayerData();
        tempPlayer.setConnectedToServer(false);
        tempPlayer.setReadyToSendWarriorCode(false);
    }

    /**
     * create a new CwLogin message that stores the name and role of the
     * client and send this to the server we are connected with
     */
    public void sendLogin() {
        CwLoginContent cwLogin = new CwLoginContent();
        HandleServerAnswers tempHandle = getHandleServerAnswers();
        PlayerData tempPlayer = tempHandle.getPlayerData();

        cwLogin.name = tempPlayer.getName();
        cwLogin.role = tempPlayer.getRole();
        cwClient.sendLoginTelegram(cwLogin);

        System.out.print("sent login\n");
    }

    /**
     * create a new CwReady message that stores the warrior code of the
     * client and send this to the server we are connected with
     */
    public void sendReady() {
        CwReadyContent cwReady = new CwReadyContent();
        HandleServerAnswers tempHandle = getHandleServerAnswers();
        PlayerData tempPlayer = tempHandle.getPlayerData();

        if (tempPlayer.getWarriorCode().isEmpty()) {
            throw new IllegalStateException("warrior code list is empty!");
        }

        cwReady.warriorRedcode = tempPlayer.getWarriorCode();
        cwClient.sendReadyTelegram(cwReady);

        System.out.print("sent ready\n");
    }
}
