package gone.client.controller.simulation;

import gone.client.model.visual.CoreContentChange;
import gone.client.model.visual.CwColor;
import gone.client.model.visual.IpOwner;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * Runnable is called to solve our thread problem to draw
 * UI updates correctly. UpdateSimulation updates the visual
 * core with the recent changes
 *
 * @author winfried
 */
public class UpdateSimulation implements Runnable {
    private TilePane coreView;
    private CoreContentChange coreContentChange;

    /**
     * constructor
     * @param coreView the javaFX object on which the core gets drawn (rectangle)
     * @param coreContentChange the change variable representing the update that has to be drawn
     */
    public UpdateSimulation(TilePane coreView, CoreContentChange coreContentChange) {
        this.coreView = coreView;
        this.coreContentChange = coreContentChange;
    }

    @Override
    public void run() {
        updateElement();
    }

    /**
     * update one single element of the TilePane with a new fill color and
     * possibly a new border color, which depends on the values of CoreContentChange
     */
    private void updateElement() {
        try {
            Paint color = coreContentChange.changedCoreContent.getColorToElement().getColor();
            ((Rectangle)coreView.getChildren().get(coreContentChange.indexOfChange)).setFill(color);

            setInstructionPointer(coreContentChange.indexOfChange,
            coreContentChange.changedCoreContent.getIpOwner());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * update the border color of one element of the TilePane, which represents
     * the type (owner) of the instruction pointer of that explicit element
     * @param index the position of the change at the core
     * @param ipOwner the enum value that represents the instruction pointer type (owner)
     */
    private void setInstructionPointer(int index, IpOwner ipOwner) {
        if (ipOwner.equals(IpOwner.NEUTRAL)) {
            ((Rectangle)coreView.getChildren().get(index)).setHeight(10);
            ((Rectangle)coreView.getChildren().get(index)).setWidth(10);
            ((Rectangle)coreView.getChildren().get(index)).setStrokeWidth(0);
        } else {
            Paint color = ipOwner.equals(IpOwner.PLAYER1)
                    ? CwColor.IP1.getColor() : CwColor.IP2.getColor();

            ((Rectangle)coreView.getChildren().get(index)).setHeight(9);
            ((Rectangle)coreView.getChildren().get(index)).setWidth(9);
            ((Rectangle)coreView.getChildren().get(index)).setStroke(color);
            ((Rectangle)coreView.getChildren().get(index)).setStrokeWidth(3);
        }
    }
}
