package gone.client.controller.simulation;

import gone.client.model.visual.CwColor;
import javafx.scene.control.Label;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Rectangle;

/**
 * Runnable is called to solve our thread problem to draw
 * UI updates correctly. InitSimulation initially draws the whole visual core
 *
 * @author winfried
 */
public class InitSimulation implements Runnable {
    private final int size;
    private final TilePane coreView;
    private Label waitLabel;

    /**
     * constructor
     * @param size size of the visual core that has to be drawn
     * @param coreView the javaFX object on which the core gets drawn (rectangle)
     */
    public InitSimulation(int size, TilePane coreView, Label waitLabel) {
        this.size = size;
        this.coreView = coreView;
        this.waitLabel = waitLabel;
    }

    @Override
    public void run() {
        initCoreView();
    }

    /**
     * simply create (value)-times black rectangles (CORE_DAT)
     */
    private void initCoreView() {
        try {
            waitLabel.setText("Spiel gestartet");

            for (int i = 0; i < size; i++) {
                Rectangle rect = new Rectangle();
                rect.setFill(CwColor.CORE_DAT.getColor());
                rect.setWidth(coreView.getPrefTileWidth());
                rect.setHeight(coreView.getPrefTileHeight());

                coreView.getChildren().add(rect);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
