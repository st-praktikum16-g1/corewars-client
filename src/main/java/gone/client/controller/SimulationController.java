package gone.client.controller;

import gone.client.controller.simulation.InitSimulation;
import gone.client.controller.simulation.UpdateSimulation;
import gone.client.model.network.ConnectionSetup;
import gone.client.model.visual.CoreContentChange;
import gone.client.model.visual.CwColor;
import gone.client.model.visual.IpOwner;
import gone.client.view.SceneLoader;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * Created by benedikt9 on 28.09.16.
 */
public class SimulationController implements Observer, Initializable {
    @FXML private TilePane coreView;
    @FXML private Label player1Label;
    @FXML private Label player2Label;
    @FXML private Label coreSizeLabel;
    @FXML private Label maxCyclesCountLabel;
    @FXML private Label currentCyclesCountLabel;
    @FXML private Label waitLabel;

    private boolean isInitialized;

    @FXML private void cancel() throws IOException, IllegalAccessException {
        ConnectionSetup.getInstance().getHandleServerAnswers().getVisualizationCore().deleteObserver(this);
        ConnectionSetup.getInstance().disconnect();

        FXMLLoader fxmlLoader = SceneLoader.loadFxml(getClass()
                .getResource("/gone/client/view/Login.fxml"));
        ((BorderPane) SceneLoader.rootLayout).setCenter(fxmlLoader.load());
    }

    @Override
    public void update(Observable observable, Object arg) {
        if (!this.isInitialized) {
            try {
                Runnable init = new InitSimulation(ConnectionSetup.getInstance()
                        .getHandleServerAnswers().getVisualizationCore().getSize(), coreView, waitLabel);
                Platform.runLater(init);
            } catch (Exception exc) {
                exc.printStackTrace();
            }

            this.isInitialized = true;
        }

        List<CoreContentChange> changesForOutput = (List<CoreContentChange>) arg;

        for (CoreContentChange change : changesForOutput) {
            Runnable update = new UpdateSimulation(coreView, change);
            Platform.runLater(update);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            ConnectionSetup.getInstance().getHandleServerAnswers().getVisualizationCore().addObserver(this);
        } catch (IllegalAccessException | UnknownHostException exc) {
            exc.printStackTrace();
        }
    }
}
