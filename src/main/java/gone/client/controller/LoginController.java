package gone.client.controller;

import gone.client.controller.MainController;
import gone.client.model.Constants;
import gone.client.model.contracts.RcFileLoader;
import gone.client.model.network.ConnectionSetup;
import gone.client.model.network.HandleServerAnswers;
import gone.client.view.SceneLoader;
import gone.lib.common.ClientRole;

import gone.lib.common.Language;
import gone.lib.common.RcStandard;
import gone.lib.config.ClientConfig;
import gone.lib.config.ConfigTypes;
import gone.lib.config.LoadConfigFile;
import gone.lib.config.SaveConfigFile;
import javafx.beans.property.ReadOnlyProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.prefs.InvalidPreferencesFormatException;

/**
 * LoginController controls the Login view which is the default scene that appears at the start
 * of the application.
 *
 * @author Benedikt Geißler
 */
public class LoginController implements Initializable {
    @FXML private ListView serverListView;
    @FXML private TextField serverHostInput;
    @FXML private Spinner<Integer> serverPortInput;
    @FXML private TextField warriorNameInput;
    @FXML private CheckBox spectatorInput;
    @FXML private ToggleGroup redcodeVersion;
    @FXML private RadioButton icws88Input;
    @FXML private RadioButton icws94Input;
    @FXML private Button loadWarriorButton;
    @FXML private Label loadedWarriorPathLabel;
    @FXML private Button setReadyButton;

    private final static String defaultConfigPath = System.getProperty("user.home") + File.separator
            + "gone.corewars" + File.separator + "config" + File.separator + "client" + File.separator
            + "defaultClientConfig.xml";
    private final static String userConfigPath = System.getProperty("user.home") + File.separator
            + "gone.corewars" + File.separator + "config" + File.separator + "client" + File.separator
            + "userClientConfig.xml";

    /*@FXML
    private void showLoginForm() {
        Stage loginStage = new Stage();
        loginStage.setWidth(320);
        loginStage.setHeight(240);
        loginStage.setTitle("Login");

        FXMLLoader fxmlLoader = SceneLoader.loadFxml(getClass()
                .getResource("../view/Login.fxml"));
        Parent parent = new GridPane();
        try {
            parent = fxmlLoader.load();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
        Scene scene = new Scene(parent, 320, 240);
        loginStage.setScene(scene);
        loginStage.showAndWait();
    }*/

    @FXML
    private void login() throws IOException {
        if (warriorNameInput.getText() == null || warriorNameInput.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, SceneLoader.getLocaleString("login.failed.no-warrior-name"),
                    ButtonType.OK);
            alert.showAndWait();
        } else if (serverHostInput.getText() == null || serverHostInput.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, SceneLoader.getLocaleString("login.failed.no-host"),
                    ButtonType.OK);
            alert.showAndWait();
        } else {
            ConnectionSetup.getInstance().getHandleServerAnswers().getPlayerData()
                    .setName(warriorNameInput.getText());
            ConnectionSetup.getInstance().getHandleServerAnswers().getPlayerData()
                    .setRole(spectatorInput.isSelected() ? ClientRole.Viewer : ClientRole.Player);

            ConnectionSetup.getInstance().getHandleServerAnswers().getPlayerData()
                    .setServerAddress(serverHostInput.getText());
            ConnectionSetup.getInstance().getHandleServerAnswers().getPlayerData()
                    .setServerPort(Integer.parseInt(serverPortInput.getEditor().getText()));

            try {
                ConnectionSetup.getInstance().connect();
                ConnectionSetup.getInstance().sendLogin();

                loadWarriorButton.setDisable(false);
            } catch (Exception exception) {
                Alert alert = new Alert(Alert.AlertType.ERROR,
                        SceneLoader.getLocaleString("login.failed.details"),
                        ButtonType.OK);
                alert.showAndWait();
            }
        }
        saveUserConfig();
    }

    @FXML
    private void showRedcodeFileChooser() throws IOException, IllegalAccessException {
        File file = SceneLoader.showFileChooser();
        if (file != null) {
            RcFileLoader rcFileLoader = null;
            rcFileLoader = new RcFileLoader(Constants.MAX_FILE_LENGTH, file);
            ConnectionSetup.getInstance().getHandleServerAnswers()
                    .getPlayerData().setWarriorCode(rcFileLoader.getInputData());

            loadedWarriorPathLabel.setText(file.getCanonicalPath());
            setReadyButton.setDisable(false);
            loadedWarriorPathLabel.setText(file.getCanonicalPath());
        }
    }

    @FXML
    private void setReady() throws IOException {
        if (!ConnectionSetup.getInstance().getHandleServerAnswers().isCoreIsInitiated()) {
            SceneLoader.showAlert(SceneLoader.getLocaleString("login.failed.no-login-ok"),
                    SceneLoader.getLocaleString("login.failed"));
        } else {
            ConnectionSetup.getInstance().sendReady();

            FXMLLoader fxmlLoader = SceneLoader.loadFxml(getClass()
                    .getResource("/gone/client/view/Simulation.fxml"));
            ((BorderPane) SceneLoader.rootLayout).setCenter(fxmlLoader.load());
        }
        //saveUserConfig();
    }

    private void saveDefaultConfig() throws IOException {
        if (new File(defaultConfigPath).exists()) {
            return;
        }

        ClientConfig config;
        config =  new ClientConfig(800, 600, "warrior", System.getProperty("user.home"),
                InetAddress.getByName("127.0.0.1"), 5556, RcStandard.ICWS88, Language.de);
        new SaveConfigFile(config, defaultConfigPath, ConfigTypes.CLIENT_CONFIG);
    }

    private void saveUserConfig() throws IOException {

        String warriorNameTemp = warriorNameInput.getCharacters().toString();
        InetAddress serverAddressTemp =  InetAddress.getByName(
                serverHostInput.getText());
        int portTemp = Integer.parseInt(serverPortInput.getEditor().getText());

        ClientConfig config;
        config =  new ClientConfig(800, 600, warriorNameTemp, System.getProperty("user.home"),
                serverAddressTemp, portTemp, RcStandard.ICWS88, Language.de);
        new SaveConfigFile(config, userConfigPath, ConfigTypes.CLIENT_CONFIG);
    }

    private ClientConfig loadConfig(String defaultPath, String userPath) throws IOException,
            InvalidPreferencesFormatException, IllegalAccessException {
        LoadConfigFile load = new LoadConfigFile();
        load.loadClientPrefs(defaultPath, userPath, ConfigTypes.CLIENT_CONFIG);
        return (ClientConfig) load.getConfigParameters();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            saveDefaultConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ClientConfig clientConfig = null;
        try {

            if (!new File(userConfigPath).exists()) {
                clientConfig = loadConfig(defaultConfigPath, defaultConfigPath);
            } else {
                clientConfig = loadConfig(userConfigPath, userConfigPath);
            }
        } catch (IOException | InvalidPreferencesFormatException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (clientConfig != null) {
            serverHostInput.setText(clientConfig.getInetAddress().getHostAddress());
            warriorNameInput.setText(clientConfig.getWarriorName());
            serverPortInput.setValueFactory(new SpinnerValueFactory
                    .IntegerSpinnerValueFactory(1024, 65535, clientConfig.getPortLastServer()));
        } else {
           serverPortInput.setValueFactory(new SpinnerValueFactory
                   .IntegerSpinnerValueFactory(1024, 65535, 5556));
        }

        try {
            saveUserConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
