package gone.client.controller;

import gone.client.model.Constants;
import gone.client.model.contracts.PlayerData;
import gone.client.model.contracts.RcFileLoader;
import gone.client.model.network.ConnectionSetup;
import gone.client.view.SceneLoader;
import gone.lib.common.ClientRole;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;

/**
 * MainController is a class that controls the root layout. The root layout is a BorderPane
 * with only a upper, center and lower part. The upper part is a MenuBar, the center part is
 * meant to be filled with a Pane controlled by LoginController or SimulationController and the
 * lower part is a status bar.
 */
public class MainController implements Initializable {

    @FXML
    private void setEnglishLanguage() {
    }

    @FXML
    private void setGermanLanguage() {

    }

    @FXML
    private void showRedcodeFileChooser() throws IOException, IllegalAccessException {
        File file = SceneLoader.showFileChooser();
        if (file != null) {
            RcFileLoader rcFileLoader = null;
            rcFileLoader = new RcFileLoader(Constants.MAX_FILE_LENGTH, file);
            rcFileLoader.getInputData();
        }
    }

    @FXML
    private void quitApplication() {
        try {
            ConnectionSetup.getInstance().disconnect();
        } catch (UnknownHostException exc) {
            exc.printStackTrace();
        }
        SceneLoader.primaryStage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
